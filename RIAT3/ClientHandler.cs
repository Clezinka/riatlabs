﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Net;
using System.Reflection;

namespace RiatLab3
{
    public class ClientHandler
    {
        public string RequestURI { get; private set; }
        private string Request { get; set; }
        public bool Power { get; set; }
        private Output OutputObject;
        private Input InputObject;
        private TcpClient Client;
        public ClientHandler()
        {
            Power = true;
        }
        private string ReadMessage()
        {
            byte[] MessageBuffer = new byte[Client.ReceiveBufferSize];
            int MessageLength;
            try
            {
                MessageLength = Client.GetStream().Read(MessageBuffer, 0, MessageBuffer.Length);
                return (MessageLength > 0) ? Encoding.UTF8.GetString(MessageBuffer, 0, MessageLength) : null;
            }
            catch
            {
                Client.Close();
                return null;
            }
        }
        public void Start(TcpClient NewClient)
        {
            Client = NewClient;
            Request = ReadMessage();
            if (!Client.Connected) return;
            Match ReqMatch = Regex.Match(Request, @"^\w+\s+([^\s\?]+)[^\s]*\s+HTTP/.*|");
            if (ReqMatch == Match.Empty)
            {
                SendError(400);
                return;
            }
            string RequestUri = String.Empty;
            RequestUri = ReqMatch.Groups[1].Value;
            RequestUri = Uri.UnescapeDataString(RequestUri);
            if (RequestUri.IndexOf("..") >= 0)
            {
                SendError(400);
                return;
            }
            try
            {
                MethodInfo Method = this.GetType().GetMethod(RequestUri.Substring(1));
                Method.Invoke(this, null);
            }
            catch
            {
                SendError(404);
                return;
            }
        }
        private void SendError(int ErrorCode)
        {
            string Header = String.Format("HTTP/1.1 {0} {1}\n\n", ErrorCode, (HttpStatusCode)ErrorCode);
            byte[] buf = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(buf, 0, buf.Length);
            Client.Close();
        }

        private void Ping()
        {
            string Header = "HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:0\n\n";
            byte[] Buffer = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(Buffer, 0, Buffer.Length);
            Client.Close();
        }
        private void GetAnswer()
        {
            OutputObject = new Output(InputObject);
            Ser Serializer = new Json();
            string JsonObject = Serializer.Serialize(OutputObject);
            string Header = String.Format("HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:{0}\n\n{1}", JsonObject.Length, JsonObject);
            byte[] OutputBytes = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(OutputBytes, 0, OutputBytes.Length);
            Client.Close();
        }
        private void PostInputData()
        {
            try
            {
                if (Request.IndexOf("Expect: 100-continue") > 0)
                {
                    Request = Expect100Handler();
                    if (!Client.Connected) return;
                }
                string InputData = String.Empty;
                Ser Serializer = new Json();
                InputData = Request;
                InputObject = Serializer.Deserialize<Input>(InputData);
                string Header = "HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:0\n\n";
                byte[] ByteBuffer = Encoding.UTF8.GetBytes(Header);
                Client.GetStream().Write(ByteBuffer, 0, ByteBuffer.Length);
                Client.Close();
            }
            catch
            {
                SendError(400);
            }
        }
        private void Stop()
        {
            string Header = "HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:0\n\n";
            byte[] ByteBuffer = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(ByteBuffer, 0, ByteBuffer.Length);
            Client.Close();
            Power = false;
        }
        private string Expect100Handler()
        {
            string Header = "HTTP/1.1 100 Continue\n\n";
            byte[] ByteBuffer = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(ByteBuffer, 0, ByteBuffer.Length);
            return ReadMessage();
        }
    }
}
