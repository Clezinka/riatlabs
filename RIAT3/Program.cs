﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Net.Http;
using Newtonsoft.Json;

namespace RiatLab3
{
    class Program
    {
        static void Main(string[] args)
        {
            int Port = int.Parse(Console.ReadLine());
            Server Server = new Server(Port);
            Server.Start();
        }
    }
}
