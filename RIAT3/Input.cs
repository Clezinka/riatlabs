﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RiatLab3
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Input
    {
        [JsonProperty]
        public int K { get; set; }
        [JsonProperty]
        public decimal[] Sums { get; set; }
        [JsonProperty]
        public int[] Muls { get; set; }
    }
}
