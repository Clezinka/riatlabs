﻿namespace RiatLab3
{
    interface Ser
    {
        string Serialize<T>(T Data);
        T Deserialize<T>(string Data);
    }
}
