﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;

namespace RiatLab1
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Input
    {
        [JsonProperty]
        public int K { get; set; }
        [JsonProperty]
        public decimal[] Sums { get; set; }
        [JsonProperty]
        public int[] Muls { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class Output
    {
        [JsonProperty]
        public decimal SumResult { get; set; }
        [JsonProperty]
        public int MulResult { get; set; }
        [JsonProperty]
        public decimal[] SortedInputs { get; set; }

        public Output() {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            String SType = Console.ReadLine();
            String InputString = Console.ReadLine();
            Output Output;
            Input Input;
            switch (SType)
            {
                case "Xml": 
                    XmlSerializer SerializerForInput = new XmlSerializer(typeof(Input));
                    TextReader Reader = new StringReader(InputString);
                    Input = (Input)SerializerForInput.Deserialize(Reader);
                    Output = new Output();
                    Output.SumResult = Input.Sums.Sum() * Input.K;
                    Output.MulResult = Input.Muls.Aggregate((agg, i) => agg * i);
                    Output.SortedInputs = Input.Sums.Concat(Array.ConvertAll(Input.Muls, x => (decimal)x)).ToArray();
                    Array.Sort(Output.SortedInputs);

                    XmlSerializerNamespaces XmlNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                    XmlWriterSettings XmlSettings = new XmlWriterSettings
                    {
                        Indent = true,
                        OmitXmlDeclaration = true,
                        Encoding = new UTF8Encoding(false)
                    };

                    XmlSerializer SerializerForOutput = new XmlSerializer(typeof(Output));
                    StringBuilder OutputString = new StringBuilder();
                    XmlWriter Writer = XmlWriter.Create(OutputString, XmlSettings);
                    SerializerForOutput.Serialize(Writer, Output, XmlNamespaces);
                    Console.Write(Regex.Replace(OutputString.ToString(), @"[\r\n\s]", "", RegexOptions.None));
                    break;
                case "Json":
                    Input = JsonConvert.DeserializeObject<Input>(InputString);
                    Output = new Output();
                    Output.SumResult = Input.Sums.Sum() * Input.K;
                    Output.MulResult = Input.Muls.Aggregate((agg, i) => agg * i);
                    Output.SortedInputs = Input.Sums.Concat(Array.ConvertAll(Input.Muls, x => (decimal)x)).ToArray();
                    Array.Sort(Output.SortedInputs);
                    Console.Write(JsonConvert.SerializeObject(Output));
                    break;
            }
        }
    }
}
