﻿using System.Net.Http;

namespace RiatLab2
{
    interface ClientGen
    {
        HttpResponseMessage GetData(string Task);
        HttpResponseMessage WriteData<T>(string Task, T Content);
    }
}
