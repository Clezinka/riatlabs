﻿namespace RiatLab2
{
    interface Ser
    {
        string Serialize<T>(T Data);
        T Deserialize<T>(string Data);
    }
}
