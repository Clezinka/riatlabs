﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Net.Http;
using Newtonsoft.Json;

namespace RiatLab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Client Client = new Client(Console.ReadLine());
            if (Client.GetData("/Ping").IsSuccessStatusCode)
            {
                Ser Serializer = new Json();
                string InputString = Client.GetData("/GetInputData").Content.ReadAsStringAsync().Result;
                Input InputO = Serializer.Deserialize<Input>(InputString);
                Output OutputO = new Output(InputO);
                string OuputS = Serializer.Serialize<Output>(OutputO);
                Client.WriteData<String>("/WriteAnswer", OuputS);
            }
        }
    }
}
