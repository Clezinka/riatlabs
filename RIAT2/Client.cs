﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.Http;

namespace RiatLab2
{
    public class Client
    {
        private string BaseUrl { get; set; }
        public Client(string port)
        {
            BaseUrl = String.Format("http://127.0.0.1:{0}", port);
        }
        public HttpResponseMessage GetData(string Task_)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                HttpClient.BaseAddress = new Uri(BaseUrl);
                return HttpClient.GetAsync(Task_).Result;
            }
        }
        public HttpResponseMessage WriteData<T>(string Task, T Content)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                HttpClient.BaseAddress = new Uri(BaseUrl);
                HttpContent SentHttpContent = new ByteArrayContent(BitConverter.GetBytes((dynamic)Content));
                HttpResponseMessage Response = HttpClient.PostAsync(Task, SentHttpContent).Result;
                HttpContent GivenHttpContent = Response.Content;
                return Response;
            }
        }
    }
}
