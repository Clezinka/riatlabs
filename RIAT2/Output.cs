﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RiatLab2
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Output
    {
        [JsonProperty]
        public decimal SumResult { get; set; }
        [JsonProperty]
        public int MulResult { get; set; }
        [JsonProperty]
        public decimal[] SortedInputs { get; set; }

        public Output(Input Input)
        {
            SumResult = Input.Sums.Sum() * Input.K;
            MulResult = Input.Muls.Aggregate((agg, i) => agg * i);
            SortedInputs = Input.Sums.Concat(Array.ConvertAll(Input.Muls, x => (decimal)x)).ToArray();
            Array.Sort(SortedInputs);
        }
    }
}
