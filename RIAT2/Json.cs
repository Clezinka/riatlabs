﻿using Newtonsoft.Json;

namespace RiatLab2
{
        public class Json : Ser
        {
            public string Serialize<T>(T Data)
            {
                return JsonConvert.SerializeObject(Data);
            }
            public T Deserialize<T>(string Data)
            {
                return JsonConvert.DeserializeObject<T>(Data);
            }
        }
}
